package com.example.niga_1202162376_si40int_pab_modul1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private EditText edtAlas;
    private EditText edtTinggi;
    private TextView edtHasil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    protected void hitung(View view){
        edtAlas = findViewById(R.id.textEd);
        edtTinggi =  findViewById(R.id.textEd2);
        edtHasil = findViewById(R.id.textView1);

        Integer alas = Integer.parseInt(edtAlas.getText().toString());
        Integer tinggi = Integer.parseInt(edtTinggi.getText().toString());
        Integer hasil = alas*tinggi;
        edtHasil.setText(String.valueOf(hasil));
    }
}
